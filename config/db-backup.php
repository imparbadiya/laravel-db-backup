<?php


return [

	'path' => storage_path() . '/db-dump/',

	'mysql' => [
		'dump_command_path' => storage_path() . '/db-dump/',
        'file_extension'    => 'sql'
	],
	'compress' => false,
];

